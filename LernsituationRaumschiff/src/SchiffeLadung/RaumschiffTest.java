package SchiffeLadung;

public class RaumschiffTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		//Erstes Schiff mit Ladungen
		
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Ladung l3 = new Ladung("Plasma-Waffe", 50);
		Ladung l4 = new Ladung("Borg-Schrott", 5);
		Ladung l5 = new Ladung("Rote Materie", 2);
		//Zweites Raumschiff mit Ladung
		
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		//Drittes Raumschiff mit Ladungen
		
		//Ladungen hinzufügen
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);
		
		klingonen.photonentorpedoSchiessen(romulaner); //kingonen schießt auf romulaner
		romulaner.phaserkanoneSchiessen(klingonen); //zurückschießen
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch"); //Nachricht an alle ausgeben
		klingonen.zustandRaumschiff();//gibt Zustand aus
		klingonen.ladungsverzeichnisAusgabe();//gibt Ladungen aus
		vulkanier.reparaturDurchfuehren(true, true, true, 5); //werte mit 5 Droiden auf
		vulkanier.photonentorpedosLaden(3);//Alle Torpedos nachladen
		vulkanier.ladungsverzeichnisAufräumen();//Ladungen Aufräumen
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);//2 mal Schießen
		
		System.out.println();
		//Status und Ladung Raumschiff1
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgabe();
		System.out.println();
		//Status und Ladung Raumschiff2
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgabe();
		System.out.println();
		//Status und Ladung Raumschiff2
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgabe();
		System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben());
				
	}

}
