package SchiffeLadung;

/**
 * Diese Klasse modeliert ein Raumschiff
 * @author Nico Wendt
 * @version 1.4.1 vom 23.04.2021
 *  
 */

import java.util.ArrayList;
import java.util.Random;


public class Raumschiff {
private int photonentorpedoAnzahl;
private int energieversorgungInProzent;
private int schildeInProzent;
private int huelleInProzent;
private int lebenserhaltungssystemeInProzent;
private int androidenAnzahl;
private String schiffsname;
private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

/**
 * Konstruktor f�r die Klasse Raumschiff
 *
 */
Raumschiff(){
	
}

/**
 * Vollparametrisiert Konstruktor f�r Klasse Raumschiff.
 * @param photonentorpedoAnzahl Anzahl der Photonentorpedos.
 * @param energieversorgungInProzent Energieversorgung in Prozent.
 * @param zustandSchildeInProzent Zustand der Schilde in Prozent.
 * @param zustandHuelleInProzent Zustand der H�lle in Prozent.
 * @param zustandLebenserhaltungssystemeInProzent Zustand der Lebenserhaltungssysteme in Prozent.
 * @param anzahlDroiden Anzahl der Droiden.
 * @param schiffsname Name des Schiff.
 */
Raumschiff(int photonentorpedoAnzahl,int energieversorgungInProzent,int zustandSchildeInProzent, int zustandHuelleInProzent,int zustandLebenserhaltungssystemeInProzent,int anzahlDroiden,String schiffsname){
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	this.energieversorgungInProzent = energieversorgungInProzent;
	this.schildeInProzent = zustandSchildeInProzent;
	this.huelleInProzent = zustandHuelleInProzent;
	this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
	this.androidenAnzahl = anzahlDroiden;
	this.schiffsname = schiffsname;
	
}

public int getPhotonentorpedoAnzahl() {
	return photonentorpedoAnzahl;
}

public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
}

public int getEnergieversorgungInProzent() {
	return energieversorgungInProzent;
}

public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
	this.energieversorgungInProzent = energieversorgungInProzent;
}

public int getSchildeInProzent() {
	return schildeInProzent;
}

public void setSchildeInProzent(int schildeInProzent) {
	this.schildeInProzent = schildeInProzent;
}

public int getHuelleInProzent() {
	return huelleInProzent;
}

public void setHuelleInProzent(int huelleInProzent) {
	this.huelleInProzent = huelleInProzent;
}

public int getLebenserhaltungssystemeInProzent() {
	return lebenserhaltungssystemeInProzent;
}

public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
	this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
}

public int getAndroidenAnzahl() {
	return androidenAnzahl;
}

public void setAndroidenAnzahl(int androidenAnzahl) {
	this.androidenAnzahl = androidenAnzahl;
}

public String getSchiffsname() {
	return schiffsname;
}

public void setSchiffsname(String schiffsname) {
	this.schiffsname = schiffsname;
}

/*public ArrayList<String> getBroadcastKommunikator() {
	return broadcastKommunikator;
}

public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
	this.broadcastKommunikator = broadcastKommunikator;
}

public ArrayList<Ladung> getLadungsverzeichnis() {
	return ladungsverzeichnis;
}

public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
	this.ladungsverzeichnis = ladungsverzeichnis;
}*/
/**
 * F�gt eine Ladung dem Ladungsverzeichnis hinzu.
 * @param neueLadung Objekt der Klasse Ladung.
 */
public void addLadung(Ladung neueLadung) {
	ladungsverzeichnis.add(neueLadung);
}
/**
 * Schie�t Photonentorpedo auf Raumschiff r.
 * Gibt -=*Click*=- aus wenn keine Torpedos vorhanden sind.
 * Wenn Torpedos vorhanden wird treffer(r) aufgerufen.
 * @param r Objekt der Kalsse Raumschiff.
 */
public void photonentorpedoSchiessen(Raumschiff r){
	if(photonentorpedoAnzahl <= 0) {
		nachrichtAnAlle("-=*Click*=-");
	}else {
		photonentorpedoAnzahl--;
		nachrichtAnAlle("Photonentorpedo abgeschossen!");
		treffer(r);
	}
}
/**
 * Schie�t Phaserkanone auf Raumschiff r.
 * Gibt -=*Click*=- aus wenn zuwenig Energie vorhanden ist.
 * Wenn genug Energie vorhanden ist, wird treffer(r) aufgerufen.
 * @param r Objekt der Kalsse Raumschiff.
 */
public void phaserkanoneSchiessen(Raumschiff r) {
	if(energieversorgungInProzent < 50) {
		nachrichtAnAlle("-=*Click*=-");
	}else {
		energieversorgungInProzent= energieversorgungInProzent - 50;
		nachrichtAnAlle("Phaserkanone abgeschossen!");
		treffer(r);
	}
}
/**
 * Vermerkt treffer des Raumschiff r und besch�digt Schilde, H�lle, Energie und Lebenserhaltungssysteme wenn die bedingungen erf�llt werden.
 * @param r Objekt der Kalsse Raumschiff.
 */
private void treffer(Raumschiff r) {
	r.trefferVermerken();
	r.setSchildeInProzent(r.getSchildeInProzent()-50);
	if(r.getSchildeInProzent() <= 0) {
		r.setSchildeInProzent(0);
		r.setHuelleInProzent(r.getHuelleInProzent() - 50);
		r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50);
		if(r.getHuelleInProzent()<=0) {
			r.setHuelleInProzent(0);
			r.setLebenserhaltungssystemeInProzent(0);
			nachrichtAnAlle("Lebenserhaltungssysteme wurden vernichtet!");
		}
		 
	}
}
/**
 * Die Methode bekommt eine Nachricht und scheichert sie in der ArrayList broadcastKommunikator.
 * @param ms die Nachricht die an alle gesendet werden soll.
 */
public void nachrichtAnAlle(String ms) {
	broadcastKommunikator.add(this.schiffsname + " sagt: " + ms);
}
/**
 * Gibt den broadcastKommunikator aller Raumschiffe zur�ck.
 * @return broadcastKommunikator
 */
public static ArrayList<String> eintraegeLogbuchZurueckgeben(){
	return broadcastKommunikator;
	
}
/**
 * Die Methode guckt im ladungsverzeichnis nach "Photonentorpedo" um diese nachzuladen.
 * Wenn keine torpedos vorhanden sind wird -=*Click*=- ausgegeben.
 * Wenn weniger Torpedos in der Ladung sind als nachgeladen werden sollen, wird die Maximale anzahl aus der Ladung genommen.
 * Wenn mehr Torpedos in der Ladung sind als nachgeladen werden sollen, wird nur die gew�nschte Anzahl geladen.
 * @param anzahlTorpedo die Anzahl der Torpedos die geladen werden soll.
 */
public void photonentorpedosLaden(int anzahlTorpedo) {
	int indexLadung = -1;
	for(int i = 0;i<ladungsverzeichnis.size();i++) { //gucken ob Ladung vorhanden ist
		if(ladungsverzeichnis.get(i).getBezeichnung()== "Photonentorpedo") {
			indexLadung = i;
		}
	}
	
	if(indexLadung == -1) {	//wenn Ladung nicht vorhanden gib -=*Click*=- aus
		System.out.println("Keine Photonentorpedos gefunden!");
		nachrichtAnAlle("-=*Click*=-");
	}
	//�berpr�fen ob genug Torpedos vorhanden sind
	if(indexLadung != -1 && ladungsverzeichnis.get(indexLadung).getMenge() < anzahlTorpedo) {//zuwenig Torpedos
		photonentorpedoAnzahl = photonentorpedoAnzahl + ladungsverzeichnis.get(indexLadung).getMenge();
		ladungsverzeichnis.get(indexLadung).setMenge(0);
		System.out.println(ladungsverzeichnis.get(indexLadung).getMenge() + " Photonentorpedo(s) eingesetz");
		ladungsverzeichnisAufr�umen();
	}else if(indexLadung != -1 && ladungsverzeichnis.get(indexLadung).getMenge() >= anzahlTorpedo) {//genug Torpedos vorhanden
		if(ladungsverzeichnis.get(indexLadung).getMenge() == anzahlTorpedo) {
			photonentorpedoAnzahl = photonentorpedoAnzahl + ladungsverzeichnis.get(indexLadung).getMenge();
			ladungsverzeichnis.get(indexLadung).setMenge(0);
			System.out.println(ladungsverzeichnis.get(indexLadung).getMenge() + " Photonentorpedo(s) eingesetz");
			ladungsverzeichnisAufr�umen();
		}else {
			photonentorpedoAnzahl = photonentorpedoAnzahl + ladungsverzeichnis.get(indexLadung).getMenge();
			System.out.println(ladungsverzeichnis.get(indexLadung).getMenge() + " Photonentorpedo(s) eingesetz");
			ladungsverzeichnis.get(indexLadung).setMenge(ladungsverzeichnis.get(indexLadung).getMenge()-anzahlTorpedo);
		}
	}
}
/**
 * Berechnet werte zur Reperatur f�r das Raumschiff.
 * Falls zu wenig Droiden vorhanden sind wird die Maximale Anzahl der Droiden benutzt.
 * @param schutzschilde boolean ob Schutzschilde repariert werden sollen.
 * @param energieversorgung boolean ob Energieversorgung repariert werden soll.
 * @param schiffshuelle boolean ob Schiffshuelle repariert werden soll.
 * @param anzahlDroiden die Anzahl der Droiden die Reparieren sollen.
 */
public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
	Random ran = new Random();
	int zufall = ran.nextInt(101);
	int anzahlReparieren = 0;
	int repWert = 0;
	if(schutzschilde == true) {//wieviele Werte sind auf true
		anzahlReparieren++;
	}else if(energieversorgung == true) {
		anzahlReparieren++;
	}else if (schiffshuelle == true) {
		anzahlReparieren++;
	}
	if(anzahlDroiden > androidenAnzahl) {//genug Droiden vorhanden?
		repWert = (zufall * androidenAnzahl)/anzahlReparieren;
	}else {
		repWert = (zufall * anzahlDroiden)/anzahlReparieren;
	}
	if(schutzschilde == true) {//Module reparieren
		schildeInProzent = schildeInProzent + repWert;
	}else if(energieversorgung == true) {
		energieversorgungInProzent = energieversorgungInProzent + repWert;
	}else if (schiffshuelle == true) {
		huelleInProzent = huelleInProzent + repWert;
	}
	System.out.println("Die Module wurden um " + repWert + " Punkte repariert");
}
/**
 * Gibt die Atribute des Raumschiffes auf der Konsole aus.
 */
public void zustandRaumschiff() {
	System.out.println("Schiffsname = " + schiffsname);
	System.out.println("Energieversorgung in Prozent = " + energieversorgungInProzent);
	System.out.println("Schilde in Prozent = " + schildeInProzent);
	System.out.println("H�lle in Prozent = " + huelleInProzent);
	System.out.println("Lebenserhaltungssysteme in Prozent = " + lebenserhaltungssystemeInProzent);
	System.out.println("Anzahl Photonentorpedo = " + photonentorpedoAnzahl);
	System.out.println("Anzahl Androiden = " + androidenAnzahl);
}
/**
 * Gibt das gesamte Ladungsverzeichnis auf der Konsole aus.
 */
public void ladungsverzeichnisAusgabe() {
	for(int i = 0;i<ladungsverzeichnis.size();i++) {
		System.out.printf("Bezeichnung: %s\nMenge: %d\n", ladungsverzeichnis.get(i).getBezeichnung(), ladungsverzeichnis.get(i).getMenge());
	}
}

/**
 * Gibt auf der Konsole aus, dass das Schiff getroffen wurde.
 */
public void trefferVermerken() {
	System.out.println(schiffsname + " wurde getroffen!");
}
/**
 * Geht das gesamte Ladungsverzeichnis durch und entfernt alle Ladungen mit der Menge 0.
 */
public void ladungsverzeichnisAufr�umen() {
	
	for(int i = 0;i<ladungsverzeichnis.size();i++) {
		if(ladungsverzeichnis.get(i).getMenge()==0) {
			ladungsverzeichnis.remove(i);
		}
	}
	
}

}
