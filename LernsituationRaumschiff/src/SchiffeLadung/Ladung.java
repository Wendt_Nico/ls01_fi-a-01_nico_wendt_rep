package SchiffeLadung;
/**
 * Diese Klasse modeliert ein Raumschiff
 * @author Nico Wendt
 * @version 1.4.1 vom 23.04.2021
 *  
 */
public class Ladung {
private String bezeichnung;
private int menge;

/**
 * Konstruktor f�r die Klasse Ladung
 *
 */
Ladung(){
	
}
/**
 * Vollparametrisiert Konstruktor f�r Klasse Ladung
 * @param bezeichnung Bezeichnung der Ladung
 * @param menge Menge der Ladung
 */
Ladung(String bezeichnung, int menge){
	this.bezeichnung = bezeichnung;
	this.menge = menge;
}
public String getBezeichnung() {
	return bezeichnung;
}
public void setBezeichnung(String bezeichnung) {
	this.bezeichnung = bezeichnung;
}
public int getMenge() {
	return menge;
}
public void setMenge(int menge) {
	this.menge = menge;
}


}
