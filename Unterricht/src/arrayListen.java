import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class arrayListen {
	
	public static void ausgabe(ArrayList<Integer> l) {
		for(int j = 0; j < l.size(); j++) {
			System.out.printf("myList[%2d] : %d\n", j, l.get(j));
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Nr. 1
		Random r = new Random();
		ArrayList<Integer> l = new ArrayList<>();
		for(int i = 0; i<20;i++) {
			l.add(1 + r.nextInt(9));
		}
		
		//Nr.2
		ausgabe(l);
		Scanner s = new Scanner(System.in);
		System.out.println("Welche Zahl soll gesucht werden: ");
		int suchen = s.nextInt();
		int z�hlen = 0;
		for(int k = 0; k<l.size();k++) {
			if(l.get(k) == suchen) {
				z�hlen++;
				System.out.printf("myList[%2d] : %d\n", k, l.get(k));
			}
		}
		System.out.printf("Die Zahl %d gibt es %d mal\n", suchen,z�hlen);
		
		System.out.printf("Liste nach L�schen von %d\n",suchen);
		for(int k = 0; k<l.size();k++) {
			if(l.get(k) == suchen) {
				l.remove(k);
				k--;
			}
		}
		
		ausgabe(l);
		System.out.println("Nach 0 hinzuf�gen:");
		for(int k = 0; k<l.size();k++) {
			if(l.get(k) == 5) {
				l.add(k+1, 0);
			}
		}
		ausgabe(l);
	}

}
