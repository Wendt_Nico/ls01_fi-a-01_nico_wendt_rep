
public class Pkw {
	private String marke;
	private String modell;
	private String farbe;
	private int leistung;
	private double km;
	private double geschwindigkeit;

	public Pkw(String marke1, String modell1, String farbe1) {
		marke = marke1;
		modell = modell1;
		farbe = farbe1;
		geschwindigkeit = 0.0;
	}

	public void setLeistung(int kW) {
		leistung = kW;
	}

	public double fahren(double strecke) {
		km += strecke;
		return km;
	}

	public String getMarke() {
		return marke;
	}
	public void setMarke(String marke) {
		this.marke = marke;
	}

	public String getModell() {
		return modell;
	}
	public void setModell(String modell) {
		this.modell = modell;
	}

	public String getFarbe() {
		return farbe;
	}
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public double getKmStand() {
		return km;
	}
}