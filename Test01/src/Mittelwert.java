import java.util.Random;
import java.util.Scanner;

public class Mittelwert {

	static int[] arr;
	public static void arrayErschaffen(int laenge) {
		arr = new int[laenge];
		for(int i = 0; i<arr.length;i++) {
			Random rnd = new Random(); 
			arr[i] = rnd.nextInt(100);
		}
	}
	
	public static double mittelwert(int laenge) {
		double mittel =0.0;
		for(int i = 0; i<laenge;i++) {
			mittel += arr[i];
		}
		mittel = mittel/laenge;
		return mittel;
	}
	
	public static void ausgabe(double mitte) {
		System.out.println("Der Mittelwert ist: " + mitte);
	}
	
   public static void main(String[] args) {
	   
	   Scanner tastatur = new Scanner(System.in);
	   System.out.println("Wie lang soll der Array sein:");
	   int laenge = tastatur.nextInt();
	   arrayErschaffen(laenge);
	   double mitte = mittelwert(laenge);
	   ausgabe(mitte);
      
   }
}
