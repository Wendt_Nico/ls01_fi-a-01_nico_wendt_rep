
import java.util.Scanner;

public class Leistung {
	
	public static void main(String[] args) {
		
		
		Scanner myScanner = new Scanner(System.in);
		double einzelleistung;
		double gesamtleistung;
		double gesamtstromstaerke;
		final double netzspannung = 230.0;
		final double maxStromstaerke = 16.0;
		int anzahlPCs;
		int anzahlStromkreise;
		
		
		System.out.print("\nLeistung eines PC-Arbeitsplatzes [in Watt]: ");
		einzelleistung = myScanner.nextDouble();
		System.out.print("Anzahl der PC-Arbeitspl�tze: ");
		anzahlPCs = myScanner.nextInt();
		gesamtleistung = einzelleistung * anzahlPCs;
		
		
		// Berechnung der erforderlichen Stromst�rke und der
		gesamtstromstaerke = gesamtleistung / netzspannung;
		// Anzahl der ben�tigten Stromkreise:......
		anzahlStromkreise = (int)Math.round(gesamtleistung/(netzspannung*maxStromstaerke));
		
		
		
		System.out.println("Gesamtleistung:  "+ gesamtleistung);
		System.out.println("Gesamtstromst�rke:  "+ gesamtstromstaerke);
		System.out.println("Anzahl der Stromkreise:  "+ anzahlStromkreise);
		
	}
}
