import java.util.Scanner;

public class Rom {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Gebe eine Zahl ein:");
		int zaehler = sc.nextInt();
		
		while(zaehler != 0) {
			if(zaehler>=1000) {
				System.out.print("M");
				zaehler-=1000;
			}else if(zaehler>=500) {
				System.out.print("D");
				zaehler-=500;
			}else if(zaehler>=100) {
				System.out.print("C");
				zaehler-=100;
			}else if(zaehler>=50) {
				System.out.print("L");
				zaehler-=50;
			}else if(zaehler>=10) {
				System.out.print("X");
				zaehler-=10;
			}else if(zaehler>=5) {
				System.out.print("V");
				zaehler-=5;
			}else {
				System.out.print("I");
				zaehler-=1;
			}
				
		}
	}

}
