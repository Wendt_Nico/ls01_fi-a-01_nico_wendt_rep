import java.util.Scanner;

import javax.naming.PartialResultException;

public class quicksort {

	public static void sort(int links, int rechts, int arr[]) {
		if(links < rechts) {
			int teil = partition(arr,links,rechts);
			sort(links,teil-1,arr);
			sort(teil+1,rechts,arr);
		}
	}
	public static int partition(int arr[], int begin, int end) {
	    int pivot = arr[end];
	    int i = (begin-1);
	 
	    for (int j = begin; j < end; j++) {
	        if (arr[j] <= pivot) {
	            i++;
	 
	            int swapTemp = arr[i];
	            arr[i] = arr[j];
	            arr[j] = swapTemp;
	        }
	    }
	 
	    int swapTemp = arr[i+1];
	    arr[i+1] = arr[end];
	    arr[end] = swapTemp;
	 
	    return i+1;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Gebe eine Zeichenkette ein die sortiert werden soll");
		String eingabe = sc.nextLine();
		char r[] = eingabe.toCharArray();
		int arr[] = new int[r.length];
		for(int i = 0;i<r.length;i++) {
			int zahl = (int) r[i];
			arr[i]= zahl;
		}
		sort(0,arr.length-1,arr);
		
		for(int k = 0;k<r.length;k++) {
			char buchstabe = (char) arr[k];
			r[k]= buchstabe;
		}
		for(int j = 0;j<arr.length;j++) {
			System.out.print(r[j]);
		}
	}

}
