import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Gebe eine Jahreszahl ein:");
		int jahr = sc.nextInt();
		
		if(jahr%4 == 0) {
			if(jahr%100==0) {
				if(jahr%400==0) {
					System.out.println("Ist ein Schaltjahr");
				}else
				System.out.println("Ist kein Schaltjahr");
			}else
			System.out.println("Ist ein Schaltjahr");
		}else
			System.out.println("Ist kein Schaltjahr");
			
	}

}
