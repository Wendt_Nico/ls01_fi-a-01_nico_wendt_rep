﻿import java.util.Scanner;
//Fertiges Programm bis Aufgabe 7++	Nico Wendt FI-A-01
class Fahrkartenautomat
{
	static double münzen[]= {0.05,0.1,0.2,0.5,1,2};	//verschiedenen arten der Münzen
	static int anzahlKasse[]= {99,99,99,99,99,99};	//Anzahl der Münzen in der Kasse
	static int rückgeld[]= {0,0,0,0,0,0};			//Anzahl des Rückgeldes in den verschiedenen Münzen
	static int anzahlMünzen = 0;					//Anzahl der Münzen die zurückgegeben werden
	static int eingeworfeneMünzen[]= {0,0,0,0,0,0};	//Anzahl der Münzen die eingeworfen wurden
	static String name[]= {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};//Namen der Fahrscheine
	static double preis[] = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};//Preise der Fahrkarten
	static int anzahlRohling[] = {99,99,99,99,99,99,99,99,99,99};//anzahl der Rohlinge jeder Fahrkartenart
	
	public static void admin() {	//Admin Menü
		int eingabe = 0;
		Scanner tastatur = new Scanner(System.in);
		while(eingabe != 99) {
		System.out.println("Kasseninhalt");
		System.out.print("Wert:     ");
		for(int i =0;i<münzen.length;i++) {
			System.out.printf("%-5.2f",münzen[i]);	//gibt die einzelnen Münzarten aus
		}
		System.out.println();
		System.out.print("Anzahl:   ");
		for(int j=0;j<anzahlKasse.length;j++) {
			System.out.printf("%-5d",anzahlKasse[j]);//gibt anzahl der Münzen aus
		}
		System.out.println();
		System.out.println();
		System.out.print("Nummer:   ");
		for(int f=0;f<anzahlRohling.length;f++) {	//gibt die Nummer der Fahrkarte aus
			System.out.printf("%-4d",f);
		}
		System.out.println();
		System.out.print("Rohlinge: ");
		for(int m=0;m<anzahlRohling.length;m++) {
			System.out.printf("%-4d",anzahlRohling[m]);//gibt die anzahl der Rohlinge aus
		}
		System.out.println();
		System.out.printf("%-3d %-40s\n",1,"Kasse leeren");//Menüpunkte
		System.out.printf("%-3d %-40s\n",2,"Kasse füllen");
		System.out.printf("%-3d %-40s\n",3,"Rohlinge leeren");
		System.out.printf("%-3d %-40s\n",4,"Rohlinge füllen");
		System.out.printf("%-3d %-40s\n",99,"zurück");
		
		eingabe = tastatur.nextInt();
		switch (eingabe) {	//Mögliche Eingaben prüfen
		case 1:	//macht die Kasse leer
			for(int k = 0;k<anzahlKasse.length;k++) {
				anzahlKasse[k]=0;
			}
			break;
		case 2:	//füllt Kasse mit beliebiger anzahl auf
			System.out.println("Wieviele 5 Cent stücken möchten Sie hinzufügen?");
			int cent5 = tastatur.nextInt();
			anzahlKasse[0]+=cent5;
			System.out.println("Wieviele 10 Cent stücken möchten Sie hinzufügen?");
			int cent10 = tastatur.nextInt();
			anzahlKasse[1]+=cent10;
			System.out.println("Wieviele 20 Cent stücken möchten Sie hinzufügen?");
			int cent20 = tastatur.nextInt();
			anzahlKasse[2]+=cent20;
			System.out.println("Wieviele 50 Cent stücken möchten Sie hinzufügen?");
			int cent50 = tastatur.nextInt();
			anzahlKasse[3]+=cent50;
			System.out.println("Wieviele 1 Euro stücken möchten Sie hinzufügen?");
			int euro1 = tastatur.nextInt();
			anzahlKasse[4]+=euro1;
			System.out.println("Wieviele 2 Euro stücken möchten Sie hinzufügen?");
			int euro2 = tastatur.nextInt();
			anzahlKasse[5]+=euro2;
			break;
		case 3:	//setzt Rohlinge auf 0
			for(int l = 0;l<anzahlRohling.length;l++) {
				anzahlRohling[l]=0;
			}
			break;
		case 4: //Auffüllen der Rohlinge für die Fahrkarten
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[0]);
			int rol0 = tastatur.nextInt();
			anzahlRohling[0]+=rol0;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[1]);
			int rol1 = tastatur.nextInt();
			anzahlRohling[0]+=rol1;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[2]);
			int rol2 = tastatur.nextInt();
			anzahlRohling[0]+=rol2;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[3]);
			int rol3 = tastatur.nextInt();
			anzahlRohling[0]+=rol3;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[4]);
			int rol4 = tastatur.nextInt();
			anzahlRohling[0]+=rol4;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[5]);
			int rol5 = tastatur.nextInt();
			anzahlRohling[0]+=rol5;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[6]);
			int rol6 = tastatur.nextInt();
			anzahlRohling[0]+=rol6;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[7]);
			int rol7 = tastatur.nextInt();
			anzahlRohling[0]+=rol7;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[8]);
			int rol8 = tastatur.nextInt();
			anzahlRohling[0]+=rol8;
			System.out.printf("Wieviele %s wollen sie hinzufügen?\n",name[9]);
			int rol9 = tastatur.nextInt();
			anzahlRohling[0]+=rol9;
			break;
		default:
			if(eingabe!=99) {	//gucken ob die Eingabe Falsch ist
				System.out.println("Falsche Eingabe");
			}
			break;
		}
		}
	}
	
	public static void zurückZahlen() {			//gibt Geld zurück, falls nicht genug Wechelgeld in der Kasse ist
		anzahlMünzen = 0;
		for(int i = 0;i < rückgeld.length;i++) {
			rückgeld[i]=eingeworfeneMünzen[i];	//rückgeld auf das eingeworfende Geld setzen
			eingeworfeneMünzen[i]=0;			//eingeworfendes Geld auf 0 setzen
			anzahlMünzen += rückgeld[i];		//anzahl der Münzen ermitteln
		}
		rückgeldAusgabe();						//gibt Münzen aus
	}
	
	public static boolean kassePrüfen(int stelle) {	//guckt ob genug Wechselged vorhanden ist
		if(anzahlKasse[stelle]>=rückgeld[stelle]) {
			return true;							//gibt true zurück wenn genug Geld vorhanden ist
		}else {
			if(stelle> 0) {							//guckt ob genug andere Münzen vorhanden sind um auf die Summe zu kommen
				switch (stelle) {
				case 5:
					while(anzahlKasse[5]<=rückgeld[5]) {	//wechsel Geld um
					rückgeld[5]--;
					rückgeld[4]+=2;
					anzahlMünzen++;
					}
					break;
				case 4:
					while(anzahlKasse[4]<=rückgeld[4]) {	//wechsel Geld um
					rückgeld[4]--;
					rückgeld[3]+=2;
					anzahlMünzen++;
					}
					break;
				case 3:
					while(anzahlKasse[3]<=rückgeld[3]) {	//wechsel Geld um
					rückgeld[3]--;
					rückgeld[2]+=2;
					rückgeld[1]++;
					anzahlMünzen+=2;
					}
					break;
				case 2:
					while(anzahlKasse[2]<=rückgeld[2]) {	//wechsel Geld um
					rückgeld[2]--;
					rückgeld[1]+=2;
					anzahlMünzen++;
					
					}
					break;
				case 1:
					while(anzahlKasse[1]<=rückgeld[1]) {	//wechsel Geld um
					rückgeld[1]--;
					rückgeld[0]+=2;
					anzahlMünzen++;
					}
					break;
				}
				
				kassePrüfen(stelle-1); //gucke ob die nächst kleinere Münze vorhanden ist
			}else {
				System.out.println("Kauf nicht möglich gebe Geld wieder aus"); //wenn kein Geld umgetauscht werden kann gebe eine Meldung aus
				return false;
			}
		}
		return false;
	}
	
	public static double nächsteMünze() {	//gucke welche Münze als nächstes ausgegeben werden muss
		for(int i = rückgeld.length-1;i>=0;i--) {
			if(rückgeld[i]>0) {
				rückgeld[i]--;			//entferne Münze aus dem rückgeld
				anzahlMünzen--;			//anzahl -1
				anzahlKasse[i]--;		//entferne die Münze aus der Kasse
				return münzen[i];		//gebe die nächste Münze aus
			}
		}
		return 0;
	}
	
	public static void münzeAusgeben(double wert1) {	//gebe eine Münze auf Konsole aus
		String münzName1 = "";
		int münzWert1 = 0;
		if(wert1 < 1) {		//gucke ob Euro oder Cent
			münzName1 = "CENT";
			münzWert1 = (int) (wert1 *100);
		}else {
			münzName1 = "EURO";
			münzWert1 = (int) wert1;
		
		}	//gebe eine Münze auf der Konsole in der Reihe aus
		System.out.printf("%4c%2c%2c\n",'*','*','*');
		System.out.printf("%2c%8c\n",'*','*');
		System.out.printf("%-5c%-5d%c\n",'*',münzWert1,'*');
		System.out.printf("%-4c%s%3c\n",'*',münzName1,'*');
		System.out.printf("%2c%8c\n",'*','*');
		System.out.printf("%4c%2c%2c\n",'*','*','*');
		
	}
	public static void münzeAusgeben(double wert1, double wert2) { // gebe 2 Münzen auf der Konsole aus
		String münzName1 = "";
		int münzWert1 = 0;
		String münzName2 = "";
		int münzWert2 = 0;
		if(wert1 < 1) { //gucke ob erste Münze Cent oder Euro ist
			münzName1 = "CENT";
			münzWert1 = (int) (wert1 *100);
		}else {
			münzName1 = "EURO";
			münzWert1 = (int) wert1;
		}
		if(wert2 < 1) { //gucke  ob zweite Münze Cent oder Euro ist
			münzName2 = "CENT";
			münzWert2 = (int) (wert2 *100);
		}else {
			münzName2 = "EURO";
			münzWert2 = (int) wert2;
		}//gebe zwei Münzen in der Reihe auf der Konsole aus
		System.out.printf("%4c%2c%2c%8c%2c%2c\n",'*','*','*','*','*','*');
		System.out.printf("%2c%8c%4c%8c\n",'*','*','*','*');
		System.out.printf("%-5c%-5d%-2c%-5c%-5d%c\n",'*',münzWert1,'*','*',münzWert2,'*');
		System.out.printf("%-4c%-6s%-2c%-4c%s%3c\n",'*',münzName1,'*','*',münzName2,'*');
		System.out.printf("%2c%8c%4c%8c\n",'*','*','*','*');
		System.out.printf("%4c%2c%2c%8c%2c%2c\n",'*','*','*','*','*','*');
		
	}
	public static void münzeAusgeben(double wert1, double wert2, double wert3) { //gebe 3 Münzen auf der Konsole aus
		String münzName1 = "";
		int münzWert1 = 0;
		String münzName2 = "";
		int münzWert2 = 0;
		String münzName3 = "";
		int münzWert3 = 0;
		if(wert1 < 1) { //gucke ob erste Münze Cent oder Euro ist
			münzName1 = "CENT";
			münzWert1 = (int) (wert1 *100);
		}else {
			münzName1 = "EURO";
			münzWert1 = (int) wert1;
		}
		if(wert2 < 1) { //gucke ob zweite Münze Cent oder Euro ist
			münzName2 = "CENT";
			münzWert2 = (int) (wert2 *100);
		}else {
			münzName2 = "EURO";
			münzWert2 = (int) wert2;
		}
		if(wert3 < 1) { //gucke ob dritte Münze Cent oder Euro ist
			münzName3 = "CENT";
			münzWert3 = (int) (wert3 *100);
		}else {
			münzName3 = "EURO";
			münzWert3 = (int) wert3;
		}//gebe drei Münzen in einer Reihe auf der Konsole aus
		System.out.printf("%4c%2c%2c%8c%2c%2c%8c%2c%2c\n",'*','*','*','*','*','*','*','*','*');
		System.out.printf("%2c%8c%4c%8c%4c%8c\n",'*','*','*','*','*','*');
		System.out.printf("%-5c%-5d%-2c%-5c%-5d%-2c%-5c%-5d%c\n",'*',münzWert1,'*','*',münzWert2,'*','*',münzWert3,'*');
		System.out.printf("%-4c%-6s%-2c%-4c%-6s%-2c%-4c%s%3c\n",'*',münzName1,'*','*',münzName2,'*','*',münzName3,'*');
		System.out.printf("%2c%8c%4c%8c%4c%8c\n",'*','*','*','*','*','*');
		System.out.printf("%4c%2c%2c%8c%2c%2c%8c%2c%2c\n",'*','*','*','*','*','*','*','*','*');
		
	}
	
	public static void fahrkarteninformationen() { //gebe das Menü aus
		System.out.println("\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		for(int i = 0; i<name.length;i++) {
			System.out.printf("%-3d %-40s %-5.2f %-3s\n",i+1,name[i],preis[i],"€");	//Ausgabe: Nr Name Preis
		}
		System.out.printf("%-3d %-40s\n",42,"Admin Menü");	//Option für das Admin Menü
		System.out.printf("%-3d %-40s\n",99,"Bezahlen");	//Option zum Bezahlen
		System.out.print("\nIhre Auswahl:");	
	}
	
	public static int anzahlFahrkarten(int wahl) { //fragt wieviele Fahrkarten geholt werden (max 10)
		Scanner tastatur = new Scanner(System.in);
		byte anzahlFahrkarten;
		System.out.print("Anzahl der Fahrkarten (Max 10): ");
	       anzahlFahrkarten = tastatur.nextByte();
	       while(anzahlFahrkarten <1 || anzahlFahrkarten >10) {	//fragt solange bis ein gültiger Wert eingegeben wird
	    	   System.out.println("Falsche Fahrkarten eingabe!");
	    	   System.out.print("Anzahl der Fahrkarten (Max 10): ");
		       anzahlFahrkarten = tastatur.nextByte();
	       }
	       if(anzahlFahrkarten<=anzahlRohling[wahl-1]) {		//guckt ob genug Rohlinge vorhanden sind
	    	   anzahlRohling[wahl-1] = anzahlRohling[wahl-1]-anzahlFahrkarten;
	    	   return anzahlFahrkarten;
	       }
	       
	       System.out.println("Nicht genug Rohlinge vorhanden!");//gibt Nachricht aus wenn nicht genug Rohlinge vorhanden sind
	       return 0;											//setzt Anzahl auf 0
	}
	
	public static double fahrkartenbestellungErfassen(){ //guckt welche Option im Menü gewählt wurde
		double zuZahlenderBetrag = 0;
		
		Scanner tastatur = new Scanner(System.in);
		int wahl = 0;
		while (wahl != 99) { //hört erst auf wenn man bezahlen möchte
		System.out.printf("Noch zu zahlen: %.2f €\n",zuZahlenderBetrag);	
		fahrkarteninformationen(); //gibt das Menü aus
		
		wahl = tastatur.nextInt();
		switch (wahl) { //guckt was gewählt wurde und berechnet den Preis
		case 1:
			
		       zuZahlenderBetrag += preis[0]*anzahlFahrkarten(wahl);
			break;
		case 2:
			
		       zuZahlenderBetrag += preis[1]*anzahlFahrkarten(wahl);
			break;
		case 3:
			
		       zuZahlenderBetrag += preis[2]*anzahlFahrkarten(wahl);
			break;
		case 4:
			
		       zuZahlenderBetrag += preis[3]*anzahlFahrkarten(wahl);
			break;
		case 5:
			
		       zuZahlenderBetrag += preis[4]*anzahlFahrkarten(wahl);
			break;
		case 6:
			
		       zuZahlenderBetrag += preis[5]*anzahlFahrkarten(wahl);
			break;
		case 7:
			
		       zuZahlenderBetrag += preis[6]*anzahlFahrkarten(wahl);
			break;
		case 8:
			
		       zuZahlenderBetrag += preis[7]*anzahlFahrkarten(wahl);
			break;
		case 9:
			
		       zuZahlenderBetrag += preis[8]*anzahlFahrkarten(wahl);
			break;
		case 10:
			
		       zuZahlenderBetrag += preis[9]*anzahlFahrkarten(wahl);
			break;
		default:
			if(wahl == 99) { //guckt ob bezahlt werden möchte
				
			}else if(wahl == 42) {	//guckt ob man in das Admin Menü möchte
				admin();//ruft das Admin Menü auf
			}else {
				System.out.println("Ich weise hier freundlich darauf hin das diese Eingabe falsch ist :)");//gibt Meldung bei Falscher Eingabe aus
			}
			break;
		}
		
		}
		return zuZahlenderBetrag;
	}
	
	public static double fahrkartenBezahlen(double zuZahlen){	//berechnet den noch zu bezahlenden Preis
		double ergebnis=0.0;
		double eingeworfeneMünze = 0.0;
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("Noch zu zahlen: %.2f €\n" , ((zuZahlen) - eingeworfeneMünze));	//gibt den betrag aus der bezahlt werden muss
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
 	   eingeworfeneMünze = tastatur.nextDouble();
 	   if(eingeworfeneMünze == 0.05) {	//guckt welche Münze eingeworfen wurde und addiert diese in der Kasse und bei den eingeworfenden Münzen
 		  ergebnis = eingeworfeneMünze;
 		  anzahlKasse[0]++;
 		  eingeworfeneMünzen[0]++;
 	   }else if(eingeworfeneMünze == 0.1) {
  		  ergebnis = eingeworfeneMünze;
  		  anzahlKasse[1]++;
		  eingeworfeneMünzen[1]++;
  	   }else if(eingeworfeneMünze == 0.2) {
  		  ergebnis = eingeworfeneMünze;
  		  anzahlKasse[2]++;
		  eingeworfeneMünzen[2]++;
  	   } else if(eingeworfeneMünze == 0.5) {
  		  ergebnis = eingeworfeneMünze;
  		  anzahlKasse[3]++;
		  eingeworfeneMünzen[3]++;
  	   } else if(eingeworfeneMünze == 1) {
  		  ergebnis = eingeworfeneMünze;
  		  anzahlKasse[4]++;
		  eingeworfeneMünzen[4]++;
  	   }else if(eingeworfeneMünze == 2) {
  		  ergebnis = eingeworfeneMünze;
  		  anzahlKasse[5]++;
		  eingeworfeneMünzen[5]++;
  	   }else 
  		   System.out.println("Das ist eine falsche Münze, die gebe ich dir gerne wieder zurück");
        //gibt eine Meldung bei falschen Münzen aus
        
		return ergebnis;
	}
	
	public static void fahrkartenAusgeben(){ //gibt die Fahrkarten aus
		  System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          warte(250);
	       }
	       System.out.println("\n\n");
	}
	
	public static void rückgeldZählen(double rückgabebetrag){ //zählt das Rückgeld
		if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n" ,rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	    	   rückgabebetrag = Math.round(rückgabebetrag*100.0)/100.0;
	    	   //Rückgeld wird gespeichert und anzahl der Münzen wird erhöht
	    	   
	           while(rückgabebetrag >= 2.0){ // 2 EURO-Münzen
	        	   anzahlMünzen++;
	        	   rückgeld[5]++;
	       	   rückgabebetrag = Math.round((rückgabebetrag - 2.0)*100)/100.00;	//rückgeldgabe wird nicht richtig berechnet, also muss gerundet werden um den richtigen Betrag zu bekommen   
	           }
	           while(rückgabebetrag >= 1.0){ // 1 EURO-Münzen
	        	   anzahlMünzen++;
	        	   rückgeld[4]++;
	       	   rückgabebetrag = Math.round((rückgabebetrag - 1.0)*100)/100.00;	//rückgeldgabe wird nicht richtig berechnet, also muss gerundet werden um den richtigen Betrag zu bekommen
	           }
	           while(rückgabebetrag >= 0.5){ // 50 CENT-Münzen
	        	   anzahlMünzen++;
	        	   rückgeld[3]++;
	        	   rückgabebetrag = Math.round((rückgabebetrag - 0.5)*100)/100.00;
	           }
	           while(rückgabebetrag >= 0.2){ // 20 CENT-Münzen
	        	   anzahlMünzen++;
	        	   rückgeld[2]++;
	        	   rückgabebetrag = Math.round((rückgabebetrag - 0.2)*100)/100.00;	 	        
	           }
	           while(rückgabebetrag >= 0.1) { // 10 CENT-Münzen
	        	   anzahlMünzen++;
	        	   rückgeld[1]++;
	        	   rückgabebetrag = Math.round((rückgabebetrag - 0.1)*100)/100.00;  
	           }
	           while(rückgabebetrag >= 0.05){ // 5 CENT-Münzen
	        	   anzahlMünzen++;
	        	   rückgeld[0]++;
	        	   rückgabebetrag = Math.round((rückgabebetrag - 0.05)*100)/100.00;
	           }
	       }
	}
	
	public static void rückgeldAusgabe() { //guckt nach welchen Wert die Münzen haben die ausgegeben werden
		while(anzahlMünzen>0) {			//guckt nach ob Münzen ausgegeben werden müssen
			double wert1 = 0;
			double wert2 = 0;
			double wert3 = 0;
			if(anzahlMünzen>=3) {		//gibt 3 Münzen in einer Reihe aus
				wert1 = nächsteMünze();
				wert2 = nächsteMünze();
				wert3 = nächsteMünze();
				münzeAusgeben(wert1, wert2, wert3);
			}else if(anzahlMünzen == 2) {	//gibt 2 Münzen in einer Reihe aus
				wert1 = nächsteMünze();
				wert2 = nächsteMünze();
				münzeAusgeben(wert1, wert2);
			}else if(anzahlMünzen == 1) {	//gibt 1 Münze in einer Reihe aus
				wert1 = nächsteMünze();
				münzeAusgeben(wert1);
			}
		}
		
	}
	
	public static void warte(int milli) {
		try {
			Thread.sleep(milli);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    public static void main(String[] args)
    {
      while (true) {
    	
       double zuZahlenderBetrag; 			//Datentyp double und Variable zuZahlenderBetrag
       double eingezahlterGesamtbetrag;		//Datentyp double und Variable eingezahlterGesamtbetrag
       double rückgabebetrag;				//Datentyp double und Variable rückgabebetrag

       zuZahlenderBetrag = fahrkartenbestellungErfassen(); //berechnet die Kosten der Fahrkarten

       // Geldeinwurf
       eingezahlterGesamtbetrag = 0.0;
       while(zuZahlenderBetrag > 0){ //wartet bis für die Karten Bezahl wurde
    	   double zahl = fahrkartenBezahlen(zuZahlenderBetrag);
    	   eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + zahl;
    	   zuZahlenderBetrag = zuZahlenderBetrag - zahl; //zieht die eingeworfende Münze von den Betrag ab
       }

       // Rückgeldberechnung und -Ausgabe
       rückgabebetrag = zuZahlenderBetrag * -1;
       rückgeldZählen(rückgabebetrag);

       // Fahrscheinausgabe
       if(kassePrüfen(5)==true) { //gucken ob genug Geld in der Kasse ist oder ob das Geld zurückgeben werden muss
    	   if(kassePrüfen(4)==true) {
    		   if(kassePrüfen(3)==true) {
    			   if(kassePrüfen(2)==true) {
    				   if(kassePrüfen(1)==true) {
    					   if(kassePrüfen(0)==true) {
    						   rückgeldAusgabe(); //zahl Rückgeld
    						   fahrkartenAusgeben(); //gibt Fahrkarten aus
    					       System.out.println("\nVergessen Sie nicht, den Fahrschein(e)\n"+
    					                          "vor Fahrtantritt entwerten zu lassen!\n"+
    					                          "Wir wünschen Ihnen eine gute Fahrt.\n");
    				       }  else
    				    	   zurückZahlen();
    				    	 
    			       }else
    			    	   zurückZahlen();
    			    	 
    		       }else zurückZahlen();
    		    	 
    	       }else 
    	    	   zurückZahlen();
    	    	 
           }else
        	   zurückZahlen();
    	 
       }else 
    	   zurückZahlen();
       
       //Wartet bis das Menü wieder gezeigt wird
       for (int i = 0; i < 8; i++){
          System.out.print("=");
          warte(550);
       }
       
       System.out.println();

      }
    }
}